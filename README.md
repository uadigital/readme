# Welcome

Launching point for UA Branding Assets Working Group


We meet weekly at 10a. Contact [Barret Baffert](mailto:baffert@email.arizona.edu) to be added to the meeting invite

Email for group: ua-digital@list.arizona.edu. Contact [Barret Baffert](mailto:baffert@email.arizona.edu) to be added

Box account for sharing working files: https://arizona.app.box.com/files/0/f/2552829843/BrandingDigitalAssets

Bitbucket team area for sharing code: https://bitbucket.org/account/user/uabrandingdigitalassets

Issue tracking for general issues: https://bitbucket.org/uabrandingdigitalassets/readme/issues (Issues related directly to specific code should be opened in the appropriate repository)

Slack for some discussion: https://uawebbranding.slack.com

Wiki for documentation: https://bitbucket.org/uabrandingdigitalassets/readme/wiki/Home

To be added as an editor to any of these resources, contact [Bernadine Cannon](mailto:bcannon@email.arizona.edu) or [Brett Bendickson](mailto:bbendick@email.arizona.edu) 


